unit UCAliens ;

interface
Uses Sysutils,Winapi.Windows,Vcl.Graphics;
type
    aliens=class            //nombre de la clase
        private           //dentro de private habran todas las cosas que solo se pue
        posx:word;
        posy:word;
        vida:boolean;
        alien: TBitmap;
        ancho:word;
        alto:word;
        public
          constructor create;
          function getPosx:word;
          function getPosy:word;
          procedure setPosx(pos:word);
          procedure setPosy(pos:word);
          function estaVivo:boolean;
          function getAlienTbit:TBitmap;
         //function setAlienTbit(alien:TBitmap);
          procedure matar;
          function getWidth:word;
          procedure setWidth(w:word);
          function getHeight:word;
          procedure setHeigth(h:word);
    end;

implementation
constructor aliens.create;
begin
  posx:=0;
  posy:=0;
  ancho:=60;
  alto:=60;
  vida:=true;
  alien:=TBitmap.Create;
  alien.LoadFromFile('alien.bmp');
  alien.Transparent:=true;
  alien.TransparentColor:= alien.Canvas.Pixels[1,1];
  alien.TransparentMode:=tmAuto;
  alien.Width:=ancho;
  alien.Height:=alto;
end;

function aliens.estaVivo: boolean;
begin
  result:=vida;
end;
function aliens.getAlienTbit: TBitmap;
begin
  result:=alien;
end;

function aliens.getHeight: word;
begin
  result:=alto;
end;

function aliens.getPosx:word;
  begin
     result:=posx;
  end;

function aliens.getposy:word;
begin
    result:=posy;
end;

function aliens.getWidth: word;
begin
  result:=ancho;
end;

procedure aliens.matar;
begin
  vida:=false;
  alien.Destroy;
end;

procedure aliens.setHeigth(h: word);
begin
   alto:=h;
end;

procedure aliens.setPosx(pos: word);
begin
  posx:=pos;
end;

procedure aliens.setPosy(pos: word);
begin
  posy:=pos;
end;

procedure aliens.setWidth(w: word);
begin
  ancho:=w;
end;

end.

