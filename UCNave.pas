unit UCNave;

interface
uses Sysutils,Winapi.Windows,Vcl.Graphics;
  type
    nave=class
    private
      pos:word;
      vida:boolean;
      naveTBit: TBitmap;
    public
      constructor create;
      function getPos:word;
      procedure setPos(pos:word);
      function getvida:boolean;
      function getNaveTbit:TBitmap;
    end;

implementation
constructor nave.create;
begin
  pos:=0;
  vida:=true;
  naveTbit:=TBitmap.Create;
  naveTbit.LoadFromFile('nave.bmp');
  naveTbit.Transparent:=true;
  naveTbit.TransparentColor:=naveTbit.Canvas.Pixels[1,1];
  naveTbit.TransparentMode:=tmAuto;

end;

function nave.getNaveTbit: TBitmap;
begin
  result:=naveTbit;
end;

function nave.getPos:word;
begin
  result:=pos;
end;
function nave.getvida: boolean;
begin
  result:=vida;
end;

procedure nave.setPos(pos: Word);
begin
  pos:=pos;
end;

end.
