unit UFPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,JPEG,PNGImage,UCAliens,UCNave,UCBalas;
const
    maxi=3;
    maxj=3;
type

  TForm1 = class(TForm)

    Timer1: TTimer;
    Timer2: TTimer;
    procedure TerminarClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormPaint(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
  private
    { Private declarations }
     k:integer;

     enemigos :Array[1..maxi,1..maxj] of aliens;
     der,izq,arb,abj:boolean;
     balaNave: balas;
     balaEnemigo: balas;
     naveUsuario: nave;
     alien: aliens;
     Fondo : TJPEGImage;
     xn,yn,bx,by:Integer;
     wAlien,hAlien:Integer;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  k:integer;
  l:integer;
implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
  i,j,ii, posAlienx,posAlieny:word;
begin
  k:=10;
  l:=10;
  posAlieny:=10;
  posAlienx:=10;

    balaNave:=balas.create;
    balaEnemigo:=balas.create;
    naveUsuario:=nave.Create;

    xn:=(Screen.Width-naveUsuario.getNaveTbit.Width) div 2;
    yn:=(screen.Height-naveUsuario.getNaveTbit.Height);
    fondo:= TJPEGImage.Create;
    fondo.LoadFromFile('fondo.jpg');


    for i := 1 to maxi do
    begin
      for j := 1 to maxj do
      begin
        enemigos[i,j]:=aliens.create;
        enemigos[i,j].setPosx(posAlienx);
        enemigos[i,j].setPosy(posAlieny);
        posAlienx:=posAlienx+130;
      end;
        posAlienx:=10;
        posAlieny:=posAlieny+70;
    end;
     wAlien:=enemigos[1,1].getWidth;
     hAlien:=enemigos[1,1].getHeight;
  end;

procedure TForm1.FormDblClick(Sender: TObject);
begin
   Application.Terminate;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
    iii: Integer;
    jjj: Integer;
begin
    case key of
      37: Izq:=true;
      39: Der:=true;
      38: Arb:=true;
      40: abj:=true;
      32: begin
          by:=yn-balaNave.getHeight;
          balaNave.setPosy(by);
          bx:=xn+((naveUsuario.getNaveTbit.Width-balaNave.getWidth) div 2);
          balaNave.setPosx(bx);
      end;
    end;

end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case key of
      37: Izq:=false;
      39: Der:=false;
      38: Arb:=false;
      40: abj:=false;
      32: balaNave.setEstadoBala(true);
  end;
end;

procedure TForm1.FormPaint(Sender: TObject);
var
  I,j: Integer;
begin
    canvas.StretchDraw(Rect(0,0,screen.Width,screen.Height),fondo);
    if balaNave.getEstadoBala then
    begin
      canvas.Draw(balaNave.getPosx,balaNave.getPosy,balaNave.getBalaBit);
    end;
    canvas.Draw(xn,yn,naveUsuario.getNaveTbit);

    for i := maxi downto 1 do
    begin
      for j := maxj downto 1 do
        begin
          if (enemigos[i,j].estaVivo)  then
           begin
              canvas.Draw(enemigos[i,j].getPosx,enemigos[i,j].getPosy,enemigos[i,j].getAlienTbit);
           end;
         end;
    end;

end;

procedure TForm1.TerminarClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  I: Integer;
  j: Integer;
  ii: Integer;
  jj: Integer;
  iii:integer;
  jjj:integer;

begin
      if izq then Xn:=Xn-25;
      if der then xn:=xn+25;
      if arb then yn:=yn-25;
      if abj then yn:=yn+25;
      if balaNave.getEstadoBala then balaNave.setPosy(balaNave.getPosy-35);
        Repaint;

    for I := 1 to maxi do
     begin
      for j := 1 to maxj do
          begin
           enemigos[i,j].setPosx(enemigos[i,j].getPosx+k);
          end;
     end;
    if (enemigos[1,maxj].getPosx>screen.Width-wAlien) or (enemigos[1,1].getPosx<1) then
    begin
      k:=k*-1;
        for ii := 1 to maxi do
        begin
          for jj := 1 to maxj do
          begin
            if enemigos[ii,jj].estaVivo then
            enemigos[ii,jj].setPosy(enemigos[ii,jj].getPosy+15);
          end;
        end;
    end;


    for iii := maxi downto 1 do
        begin
          for jjj := maxj downto 1 do
          begin
          if  ((balaNave.getPosx>=enemigos[iii,jjj].getPosx) AND (balaNave.getPosx<=enemigos[iii,jjj].getPosx+wAlien) AND (balaNave.getPosy<=enemigos[iii,jjj].getPosy) AND (balaNave.getPosy<=enemigos[iii,jjj].getPosy+hAlien) and (enemigos[iii,jjj].estaVivo)) then
              begin
                enemigos[iii,jjj].matar;
                balaNave.setEstadoBala(false);
                Break
              end;
          end;
        end;
end;

end.
