unit UCBalas;

interface
  uses Sysutils,Winapi.Windows,Vcl.Graphics;
    Type
      balas=class
        private
         posx:word;
         posy:word;
         bala:Tbitmap;
         estado:boolean;
         width:word;
         height:word;
        Public
          constructor create;
          function getPosx:word;
          function getPosy:word;
          procedure setPosx(pos:word);
          procedure setPosy(pos:word);
          function getBalaBit:Tbitmap;
          function getEstadoBala:boolean;
          procedure setEstadoBala(disp:boolean);
          function getWidth:word;
          procedure setWidth(w:word);
          function getHeight:word;
          procedure setHeight(h:word);


      end;
implementation
constructor balas.create;
begin
  posx:=0;
  posy:=0;
  width:=10;
  height:=20;
  bala:=TBitmap.Create;
  estado:=true;
  bala.LoadFromFile('bala.bmp');
  bala.Transparent:=true;
  bala.TransparentColor := bala.Canvas.Pixels[1,1];
  bala.TransparentMode:= tmAuto;
end;

function balas.getBalaBit: Tbitmap;
begin
  result:=bala;
end;

function balas.getEstadoBala: boolean;
begin
  result:=estado;
end;

function balas.getHeight: word;
begin
  result:=height;
end;

function balas.getPosx;
begin
  result:=posx;
end;
function balas.getPosy: word;
begin
  result:=posy;
end;


function balas.getWidth: word;
begin
   result:=width;
end;

procedure balas.setEstadoBala(disp: boolean);
begin
   estado:=disp;
end;

procedure balas.setHeight(h: word);
begin
    height:=h;
end;

procedure balas.setPosx(pos: Word);
begin
  posx:=pos;
end;

procedure balas.setPosy(pos: word);
begin
  posy:=pos;
end;

procedure balas.setWidth(w: word);
begin
    width:=w;
end;

end.
